/**
 * Asynchronous fetch from the URL
 * @param {String} URL
 * @return {json} Json fetched from the URL 
 */
const asyncFetch = async (URL) =>{

    const response = await fetch(URL)
    const data = await response.json()
    //console.log('sending data',data)
    return data;
}

/**
 *  Asynchronously search the github repositories for the search keyword
 *  @return {Array} Processed data for all the repos
 */
const search = async () =>{
    let userInput = $('#search-bar').val()    
    const reposURL = `https://api.github.com/search/repositories?q=${userInput}`
    
    if(userInput.trim() !== ""){
        let repos = await asyncFetch(reposURL)
        repos = repos.items
        //console.log('got repos',repos)
        const users = await Promise.all(repos.map(async (repo) => {
            //console.log('fetching..',repo.owner.url)
            //console.log(idx,repo.name)
            let obj = {}
            obj['name'] = repo.name
            obj['full_name'] = repo.full_name
            obj['private'] = repo.private
            // console.log('Set name')
            // json[idx].name = repo.name
            // console.log('Set full name')
            // json[idx].full_name = repo.full_name
            // console.log('Set private')
            // json[idx].private = repo.private
            
            const userInfo = await asyncFetch(repo.owner.url)
            let ownerObj = {
                login : userInfo.login,
                name: userInfo.name,
                followersCount: userInfo.followers,
                followingCount: userInfo.following

            }
            //console.log('owner object', ownerObj)
            obj['owner'] = ownerObj
            obj['licenseName'] = repo.license ? repo.license.name  : null
            obj['score'] = repo.score
            let branchesURL = repo['branches_url'].substring(0, repo['branches_url'].length - 9)
            //console.log('branches url',branchesURL)
            let branches = await asyncFetch(branchesURL)
            obj['numberOfBranch'] = branches.length

            return obj
        }))
        //console.log('when promise.all resolves we get all users info',users)
        return users
    }
    else{
        alert('Input Missing')
    }

}

/**
 *  Handle keyboard enter event
 */
$('#search-bar').keypress(async evt=>{
    let keyCode = evt.keyCode ? evt.keyCode : evt.which
    if(keyCode == '13'){
        let users = await search()
        $('#json').text(JSON.stringify(users,null,2))
    }

})

/**
 * Handle search icon click event
 */
$('#search-icon').on('click',async evt=>{  
    let users = await search()
    $('#json').text(users)
})